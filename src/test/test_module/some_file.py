"""Delete this module as soon as actual code is pushed."""


def starify(s: str) -> str:
    """Put stars around strings.

    Args:
        s: The input string.

    Returns:
        The *starified* output string.

    """
    return f'*{s}*'
