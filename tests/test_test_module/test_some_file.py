from test.test_module.some_file import starify


def test_starify():
    assert starify('abc') == '*abc*'
